/**
 * Initializing and importing all the libraries.
 *  => dataframe-js: For fetching as dataframe.
 */
const DataFrame = require('dataframe-js').DataFrame;

/**
 * Base data reader for reading static data from formats.
 * Need to be inherited by format readers.
 */
module.exports = class DataReaderBase{

    /**
     * Default constructor.
     */
    constructor(){
    }

    /**
     * Abstract method for reading data.
     */
    ReadData(){
        throw new Error('You havent implemented this method.');
    }

    /**
     * Parsing data as dataframe format.
     * @param {string} data : Raw data
     */
    DataParser(data){
        const df = new DataFrame(data);
        return df;
    }
}