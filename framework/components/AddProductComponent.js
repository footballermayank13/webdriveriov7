const reporter = require('../reporter/reporter');
const webdriver = require('../library/keywords/keywords');
const addProductPage = require('../pages/addProductPage');




class AddProduct{

    async selectProduct(){
        await webdriver.click(await addProductPage.selectProduct());
        reporter.addStep("Selected product successfully","passed");
    }

    async addToCart(){
        await webdriver.click(await addProductPage.addToCart());
        reporter.addStep("Click to add to cart","passed");
    }

    async goToCartAndCheckout(){
        await webdriver.click(await addProductPage.goToCart());
        reporter.addStep("Click to go to cart","passed");
        await webdriver.click(await addProductPage.checkOutButton());
        reporter.addStep("Click to checkout","passed");
    }

    async addDetails(){
        await webdriver.setValue(await addProductPage.addFirstName(),"auto");
        reporter.addStep("Added first name as : "+"auto","passed");
        await webdriver.setValue(await addProductPage.addLastName(),"delta");
        reporter.addStep("Added last name as : "+"delta","passed");
        await webdriver.setValue(await addProductPage.addZipCode(),"232323");
        reporter.addStep("Added zip code as : "+"232323","passed");
    }
    async continueButtonandFinishButton(){
        await webdriver.click(await addProductPage.continueButton());
        reporter.addStep("Click to continue button","passed");
        await webdriver.click(await addProductPage.finishButton());
        reporter.addStep("Click to finish button","passed");
    }

}

module.exports = new AddProduct()