/**
 * Initializing and importing all the libraries.
 *  => DataReaderBase: Importing base data reader.
 */
const ReadData = require('./data_reader_base');

/**
 * JSON file reader class.
 */
class JsonReader extends ReadData{

    /**
     * Default constructor.
     */
    constructor(){
        super();
        this.data;
    }

    /**
     * Read data from JSON file with file name provided.
     * @param {string} fileName : JSON file name.
     */
    ReadData(fileName){
        /* Fetch JSON data. */
        this.data = require('../../../test_data/' + fileName);
    }
    
    /**
     * JSOON data parser
     * @param {string} fileName : JSON file name
     * @returns {JSON} Data in JSON format
     */
    JsonParser(fileName){
        this.ReadData(fileName);
        return this.data;
    }
}

/* Export as class object. */
module.exports = new JsonReader();