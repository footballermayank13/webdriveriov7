const { Given, When, Then } = require('@wdio/cucumber-framework');
const loginPage = require('../../framework/pages/loginAndLogout.page');
const webdriver = require('../../framework/library/keywords/keywords');
const addProductcomponent = require('../../framework/components/AddProductComponent');

Given('I am on the homepage of the application', async function(){
  var text = await webdriver.getText(await loginPage.homePage());
  await webdriver.comparater(text,"Products");
});

When('Select the product', async function(){
  await addProductcomponent.selectProduct();
});

Then ('Add the product to cart', async function(){
await addProductcomponent.addToCart();
});

Then('Go to the cart for checkout', async function(){  
  await addProductcomponent.goToCartAndCheckout();
});
Then('add the details', async function(){  
  await addProductcomponent.addDetails();
});

Then('Complete the order', async function(){  
  await addProductcomponent.continueButtonandFinishButton();
});
