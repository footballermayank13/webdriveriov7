Feature: Add the selected item to cart and complete order

  Scenario Outline: As a user, I can log in to the application and able to complete the order

    Given I am on the homepage of the application
    When Select the product 
    Then Add the product to cart
    Then Go to the cart for checkout
    Then add the details 
    And Complete the order