const reporter = require('../reporter/reporter');
const webdriver = require('../library/keywords/keywords');
const loginPage = require('../pages/loginAndLogout.page');



class loginComponents {

    async launchAndLogin(){
        await browser.maximizeWindow();
        await browser.url("https://www.saucedemo.com/");
        await webdriver.setValue(await loginPage.username(),"standard_user");
        reporter.addStep("Entered username as : "+"standard_user","passed");
        await webdriver.setValue(await loginPage.password(),"secret_sauce");
        reporter.addStep("Entered password as : "+"secret_sauce","passed");
        await webdriver.click(await loginPage.loginButton());
        reporter.addStep("Clicked on login button","passed");
    }

    async logOut(){
        await webdriver.click(await loginPage.homeDropDown());
        reporter.addStep("Clicked on home page dropdown","passed");
        await webdriver.waitForExists(await loginPage.logOut(),8000);
        await webdriver.click(await loginPage.logOut());
        reporter.addStep("Clicked on logout button","passed");
    }

}
module.exports = new loginComponents();