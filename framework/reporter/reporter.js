/**
 * Initializing and importing all the libraries.
 *  => allureReporter: For accessing allure reporter.
 *  => fs-extra: File system access.
 *  => path: Path access.
 */
const allureReporter = require('@wdio/allure-reporter').default;
const { fail } = require('assert');
const fs = require('fs-extra');
const path = require('path');
const assert = require('assert/strict');

/**
 * Reporter class
 */
class Reporter {

    /**
     * HTML report : Logging in report.
     * @param {string} message: Message to be printed in report 
     */
    logMessage(message) {
        /* Add message. */
        process.emit('test:log', message);
    }
    
    /**
     * HTML report : To take screenshot.
     * @param {string} message : Screenshot title.
     */
    takeScreenshot(message) {
        /* Generating time stamp. */
        const timestamp = Date(Date.now()).replace(/ /g,'').split('(')[0].replace("+","").replace(":","");
        /* Enrusre directory */
        fs.ensureDirSync('reports/html-reports/screenshots/');
        /* Generating file path. */
        const filepath = path.join('reports/html-reports/screenshots/', timestamp + '.png');
        /* Saving screenshot to the path. */
        browser.saveScreenshot(filepath);
        /* Adding title to the screenshot in the reporter. */
        process.emit('test:screenshot', filepath);
        /* Return the screenshot instance. */
        return this;
    }

    /**
     * Add HTML report.
     * @param {string} message : Report log.
     * @param {Boolean} addScreenshot : Screenshot to be added or not.
     */
    addHtmlReport(message, addScreenshot){
        if(addScreenshot == true) {
            this.logMessage(message);
            this.takeScreenshot(message);
        } else {
            this.logMessage(message);
        }
    }

    /**
     * Reporter function to add report log.
     * @param {string} message : Report log.
     * @param {string} status : Log status: passed, failed.
     * @param {Boolean} addScreenshot : Screenshot to be added or not, true or false.
     */
    addStep(message, status, addScreenshot) {
        console.log(status + ' : ' + message);
        if(status == "passed") {
            if(addScreenshot == undefined) {
                allureReporter.addStep(message, {}, status);
                this.addHtmlReport(message);
            } else {
                addScreenshot = true;
                browser.takeScreenshot();
                allureReporter.addStep(message, {name:browser.takeScreenshot()}, status);
                this.addHtmlReport(message, addScreenshot);
            }
        } else {
            addScreenshot = true;
            browser.takeScreenshot();
            allureReporter.addStep(message, {name:browser.takeScreenshot()}, status);
            this.addHtmlReport(message, addScreenshot);
            assert.fail();
        }
    }
}

/* Export as class object. */
module.exports = new Reporter();