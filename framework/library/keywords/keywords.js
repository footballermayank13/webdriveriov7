const reporter = require("../../reporter/reporter");

const defaultTimeout = 15000;
const interval = 500;
var result = false;

class Keywords{
    async waitForExists(element, timeOut) {
        if(timeOut == undefined) {
            timeOut = defaultTimeout;
        }
        var count = 0;
        // timeOut *= 1000;
        while(count <= timeOut){
            if(await element.isExisting()) {
                result = true;
                break;
            } else {
                count += interval;
            }
        }
        if(! result) {
            console.log("Element still does't exist after " + timeOut + "ms");
        }
        return result;
    }

    /**
     * 
     * @param {string} element 
     * @param {BigInt} timeOut 
     */
    async waitForDisplayed(element, timeOut){
        if(timeOut == undefined)
            timeOut = defaultTimeout;
        await element.waitForDisplayed({timeout:timeOut});
        return true;
    }

    /**
     * 
     * @param {string} element 
     * @param {BigInt} timeOut 
     */
    async waitForClickable(element, timeOut){
        if(timeOut == undefined)
            timeOut = defaultTimeout;
        await element.waitForClickable({timeout:timeOut});
        return true;
    }

    /**
     * Click action.
     * @param {object} element : Web element location.
     * @param {BigInt} timeOut : Time to wait foro element.
     */
    async click(element, timeOut){
        if(timeOut == undefined) {
            timeOut = defaultTimeout;
        }
        if(await this.waitForExists(element, timeOut)){
            await element.click();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set value to edit field.
     * @param {object} element : Web edit field.
     * @param {string} value : Value to enter.
     * @param {BigInt} timeOut : Time to wait for element.
     */
    async setValue(element, value, timeOut){
        if(timeOut == undefined) {
            timeOut = defaultTimeout;
        }
        if(await this.waitForExists(element, timeOut)){
            await element.setValue(value);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Select value by visible text from drop down field.
     * @param {object} element : Web drop down.
     * @param {string} value : Value to enter.
     * @param {BigInt} timeOut : Time to wait for element.
     */
    async selectByVisibleText(element, value, timeOut){
        if(timeOut == undefined) {
            timeOut = defaultTimeout;
        }
        if(await this.waitForExists(element, timeOut)){
            await element.selectByVisibleText(value);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Fetches the visible text.
     * @param {Object} element : Web element.
     * @param {BigInt} timeOut : Time to appear.
     */
    async getText(element, timeOut){
        var text = "";
        if(timeOut == undefined){
            timeOut = defaultTimeout;
        }
        if(await this.waitForDisplayed(element, timeOut)){
            text = await element.getText();
            console.log("Got text : " + text);
            return text;
        } else {
            console.log("Waited for " + timeOut + "ms. But still element doesn't exists");
            return undefined;
        }
    }

    /**
     * Selects special kind of javascript based drop downs.
     * @param {Object} dropDownElement : Drop down web element.
     * @param {Object} valueElement : Option to select web element.
     * @param {BigInt} timeOut : Time to appear.
     */
    async selectByWebElement(dropDownElement, valueElement, timeOut) {
        var result = false;
        if(timeOut == undefined) {
            timeOut = defaultTimeout;
        }
        // if(await this.waitForExists(dropDownElement, timeOut)){
        //     await dropDownElement.click();
        //     result = true;
        // } 
        // await browser.pause(500);
        // if( await this.waitForExists(valueElement, timeOut)) {
        //     await valueElement.click();
        //     result = true;
        // } else {
        //     result = false;
        // }
        do {
            await dropDownElement.click();
            await browser.pause(500);
        } while (! await this.waitForExists(valueElement, 500));
        if( await this.waitForExists(valueElement, timeOut)) {
            await valueElement.click();
            result = true;
        } else {
            result = false;
        }
    }

    /**
     * Keyword to execute client side javascript commands on browser.
     * @param {string} command : Command to execute
     */
    async execute(command) {
        if(command == undefined || command == "")
            reporter.addStep("Command passed : " + command);
        else
            await browser.execute(command);
    }
    
    async comparater(actualValue,expectedValue){
        if(actualValue===expectedValue){
            reporter.addStep("Actual is Matched with expected value got value as : "+actualValue,"passed");
        }
        else {
            reporter.addStep("Actual value doest not matches the expected value got value as :"+actualValue,"failed");
        }
    }
}

module.exports = new Keywords();