const Page = require('./page');

class addProduct extends Page {

    selectProduct = async function () { return await browser.$('//*[@id="item_0_title_link"]')};
    addToCart = async function () { return await browser.$('//*[@id="add-to-cart-sauce-labs-bike-light"]')};
    goToCart = async function () { return await browser.$('//*[@id="shopping_cart_container"]')};
    checkOutButton = async function () { return await browser.$('//*[@id="checkout"]')};
    addFirstName = async function () { return await browser.$('//*[@id="first-name"]')};
    addLastName = async function () { return await browser.$('//*[@id="last-name"]')};
    addZipCode = async function () { return await browser.$('//*[@id="postal-code"]')};
    continueButton = async function () { return await browser.$('//*[@id="continue"]')};
    finishButton = async function () { return await browser.$('//*[@id="finish"]')};

        /**
     * overwrite specific options to adapt it to page object
     */
        open () {
            return super.open('login');
        }
    

}
module.exports = new addProduct();