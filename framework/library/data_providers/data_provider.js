/**
 * Initializing and importing all the libraries.
 *  => config.json: Framework configuration.
 *  => JSONReader: Data provider class for JSON datatype.
 *  => fs: Directory reader.
 */
const config = require('../../config/data-provider-config.json')
const jsonReader = require('./json_reader');
const fs = require('fs');

/**
 * Global variables for data returns.
 *  => data: Getting data bunch of array type.
 *  => dataConfig: Config file reader.
 */
var data = [];
var dataConfig = config.DataSource;

/**
 * Data reader class
 */
module.exports = class DataReader {

    /**
     * Default constructor.
     */
    constructor(testCaseName, dataCode) {
        this.testCaseName = testCaseName;
        this.dataCode = dataCode;
        this.dataConfig = "";
        if(this.testCaseName == undefined) {
            data = [];
        } else {
            if(Array.isArray(dataConfig)) {
                for(let i = 0; i < dataConfig.length; i ++) {
                    this.dataConfig = dataConfig[i];
                    if(this.dataConfig.toLowerCase() == 'json') {
                        this.readData(this.testCaseName);
                    } else if(this.dataConfig.toLowerCase() == 'database' || 
                            this.dataConfig.toLowerCase() == 'db') {
                        this.readData(this.testCaseName, this.dataCode);
                    } else if(this.dataConfig.toLowerCase() == 'csv') {
                        this.readData(this.testCaseName);
                    }
                }
            } else {
                this.dataConfig = dataConfig;
                if(this.dataConfig.toLowerCase() == 'json') {
                    this.readData(this.testCaseName);
                } else if(this.dataConfig.toLowerCase() == 'database' || 
                        this.dataConfig.toLowerCase() == 'db') {
                    this.readData(this.testCaseName, this.dataCode);
                } else if(this.dataConfig.toLowerCase() == 'csv') {
                    this.readData(this.testCaseName);
                }
            }
        }
    }

    /**
     * Reads data and returns the bufffer.
     * @param {string} dataFile : Data file path.
     * @returns {Dict} data in dictionary format.
     */
    dataSource(dataFile){
        if(this.dataConfig.toLowerCase() == 'json') {
            var data = jsonReader.DataParser(jsonReader.JsonParser(dataFile)).toDict();
            return data;
        } 
        else if(this.dataConfig.toLowerCase() == 'csv') {
            var data = csv.DataParser(dataFile).toDict();
            return data;
        }
    }

    /**
     * Get all data
     */
    getTestCaseData(){
        return data;
    }

    /**
     * Reading file content synchronously.
     */
    readData(testCaseName, dataCode){
        if(testCaseName == undefined) {
            console.log("No test case nameis provided... \nreading overall data files...");
            if(this.dataConfig.toLowerCase() == 'json') {
                console.log("Reading json");
                /* Reading file synchronously. */
                fs.readdirSync('./test_data/').forEach(eachFile => {
                    /* Check for file extension and if JSON file. */
                    if(eachFile.split('.')[eachFile.split('.').length - 1] == dataConfig.toLowerCase() 
                        && dataConfig.toLowerCase() == 'json'){
                        /* Storing data to data buffer. */
                        data.push(this.dataSource(eachFile));
                    }
                });
            }
        }
        else {
            console.log("Reading data for test case : " + testCaseName);
            if(this.dataConfig.toLowerCase() == 'database' || 
                    this.dataConfig.toLowerCase() == 'db') {
                console.log("Reading db");
                if(dataCode == undefined) {
                    if(config.TableName == undefined) throw console.error("Table name is neither provided as parameter, nor in dataprovider config... \nexiting...");
                    dataCode = config.TableName;
                }
                var result = mysql.ReadData(this.testCaseName, dataCode);
                data.push(this.dataSource(result));
            } else if(this.dataConfig.toLowerCase() == 'json') {
                data.push(this.dataSource(testCaseName));
            } else if(this.dataConfig.toLowerCase() == 'csv') {
                data.push(this.dataSource(csv.CsvParser(this.testCaseName)));
            }
        }
    }

    /**
     * Function to fetch data by key and index.
     * @param {string} dataName : Key to the json.
     * @param {string} index : Data index to be fetched.
     */
    getData(dataName, index){
        let dataValue;
        /* Loop to iterate in the data set. */
        for (let i = 0; i < data.length; i++) {
            /* Getting keys of data. */
            var dataKey = Object.keys(data[i]);
            /* Iterate on keys. */
            for(let j = 0; j < dataKey.length; j++){
                /* Check for key equals to key passed. */
                if(dataKey[j] == dataName){
                    /* Fetching index. */
                    if(index == undefined) {
                        /* return data value */
                        dataValue = data[i][dataKey[j]];
                        break;
                    }
                    /* Return data with index if index is passed. */
                    dataValue = data[i][dataKey[j]][index];
                    break;
                }
            }
        }
        /* Check for if found data with data key and index. */
        if(dataValue == undefined){
            console.log('Cannot find data with name : ' + dataName);
        } else {
            /* Return data value. */
            return dataValue;
        }
    }

    /**
     * Function to fetch data by key and index.
     * @param {string} dataName : Key to the json.
     */
    get(dataName){
        let dataValue;
        /* Loop to iterate in the data set. */
        for (let i = 0; i < data.length; i++) {
            /* Getting keys of data. */
            var dataKey = Object.keys(data[i]);
            /* Iterate on keys. */
            for(let j = 0; j < dataKey.length; j++){
                /* Check for key equals to key passed. */
                if(dataKey[j] == dataName){
                    /* return data value */
                    dataValue = data[i][dataKey[j]][0];
                    break;
                }
            }
        }
        /* Check for if found data with data key and index. */
        if(dataValue == undefined){
            console.log('Cannot find data with name : ' + dataName);
        } else {
            /* Return data value. */
            return dataValue;
        }
    }
}
