const ReadData = require('./data_reader_base');
const mysql = require('../db_handler/connect_to_sql');
const config = require('../../config/data-provider-config.json');

class DBReader extends ReadData {
    constructor() {
        super();
        this.data;
    }

    ReadData(dataRowId, tableName) {
        mysql.connect(config.HostName, config.Port, config.Username, config.Password, config.DatabaseName);
        this.data = mysql.connExecute("Select * from " + tableName + " where TestCaseName='" +  dataRowId + "'");
        mysql.connRelease();
        return this.data;
    }
}

module.exports = new DBReader();