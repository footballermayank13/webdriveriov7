

const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginandLogoutPage extends Page {
   
    username = async function () { return await browser.$('//*[@id="user-name"]')};
    password = async function () { return await browser.$('//*[@id="password"]')};
    loginPage = async function () { return await browser.$('//div[text()="Swag Labs"]')};
    loginButton = async function () { return await browser.$('//*[@id="login-button"]')};
    homePage = async function () { return await browser.$('//*[text()="Products"]')};
    logOut =  async function () { return await browser.$('//*[text()="Logout"]')};
    homeDropDown = async function() { return await browser.$('//*[@id="react-burger-menu-btn"]')};


    /**
     * overwrite specific options to adapt it to page object
     */
    open () {
        return super.open('login');
    }
}

module.exports = new LoginandLogoutPage();
